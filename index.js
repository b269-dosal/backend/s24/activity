let welcome = "Hellow World!"
console.log(`${welcome} \n\n`);



//getCube
const getCube = 2 ** 3 ;
console.log(`The cube of 2 is ${getCube}`);

//Address
const address = ["258 Washington Ave NW", "California", "90011"];
const [street, state, zip] = address;

console.log(`I live at ${street}, ${state}, ${zip}`);

//animal
const animal = {
  	name: "Lolong",
  	species: "saltwater crocodile",
  	weight: 1075,
  	measurement: ['20 ft 3 in'],
	
};
const {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He weighed ay ${weight}kgs with a measurement of ${measurement}.`);

//number array
const numbers = [1, 2, 3, 4, 5];
numbers.forEach(num => console.log(num));


//reduce number array
const reduceNumbers = [1, 2, 3, 4, 5];
const sum = reduceNumbers.reduce((accumulator, currentValue) => {
  return accumulator + currentValue;
});

console.log(`The Reduce Number Array `);
console.log(sum);

class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
console.log(myDog);

const myFaveDog = new Dog("Thor", 2, 'Rottweiler');
console.log(myFaveDog);


